## v1.6.0
Released: 2018-08-07

* Dependency updates
    * Includes security updates as recommended by `npm audit`

## v1.5.0
Released: 2018-02-20

* IPv4 addresses that are mapped into IPv6 (like `::ffff:192.168.1.1`) can now be used with the top-level `isLocalhost`, `isPrivate`, `isSpecial` and `isReserved` functions.

## v1.4.0
Released: 2018-02-15

* Handle an out-of-range IPv4 address mapped onto the IPv6 `::ffff:0:0/96` range.

## v1.3.0
Released: 2018-02-15

* Improved performance due to profiling, especially for IPv6.
* More unit tests.
